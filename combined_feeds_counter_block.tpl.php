<?php
/**
 * @file
 * Template to display combined feeds counter.
 *
 * Fields available:
 * - $block
 * - $webservices
 *
 */
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block block block-<?php print $block->module; ?>">

<?php if (!empty($block->subject)): ?>
  <h2><?php print $block->subject; ?></h2>
<?php endif;?>

  <div class="content">
    <?php print $block->content; ?>
    <?php
    $sum = 0;
    foreach ($webservices as $webservice) {
      $sum += $webservice->getFollowers();
    }
    echo $sum;
    ?>
  </div>
</div>
