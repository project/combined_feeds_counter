<?php

/**
 * @file
 * Define the WebService_ResultDelegate_Twitter class
 */

/**
 * Deal with Twitter followers
 */
class WebService_ResultDelegate_Twitter implements WebService_ResultDelegate {
  private $followers;

  /**
   *
   * @return stdClass
   */
  public function getFollowers() {
    return $this->followers;
  }

  /**
   *
   * @param stdClass $result
   * @return WebService_ResultDelegate_Twitter 
   */
  public function setFollowers(stdClass $result = NULL) {
    $this->followers = 
      (!empty($result) && isset($result->user->followers_count))
      ? $result->user->followers_count
      : 0;
    return $this;
  }
}