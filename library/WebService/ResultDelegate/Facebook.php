<?php

/**
 * @file
 * Define the WebService_ResultDelegate_Facebook class
 */

/**
 * Deal with Facebook followers
 */
class WebService_ResultDelegate_Facebook implements WebService_ResultDelegate {
  private $followers;

  /**
   *
   * @return stdClass
   */
  public function getFollowers() {
    return $this->followers;
  }

  /**
   *
   * @param stdClass $result
   * @return WebService_ResultDelegate_Facebook 
   */
  public function setFollowers(stdClass $result = NULL) {
    $this->followers = 
      (!empty($result) && isset($result->json->likes))
      ? $result->json->likes
      : 0;
    return $this;
  }
}
