<?php

/**
 * @file
 * Define the WebService_ResultDelegate_Feedburner class
 */

/**
 * Deal with Feed followers
 */
class WebService_ResultDelegate_Feedburner implements WebService_ResultDelegate {
  private $followers;

  /**
   *
   * @return stdClass
   */
  public function getFollowers() {
    return $this->followers;
  }

  public function setFollowers(stdClass $result = NULL) {
    $this->followers = 
      (!empty($result) && isset($result->rsp->feed->entry->circulation))
      ? $result->rsp->feed->entry->circulation 
      : 0;
    return $this;
  }
}