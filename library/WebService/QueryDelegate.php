<?php

/**
 * @file
 * Define the WebService_QueryDelegate class
 */

/**
 * WebService_QueryDelegate class
 *
 * Implementation of base class for Query Delegates
 */
class WebService_QueryDelegate {
  
  private $source;
  private $useTable;
  private $fields = array();
  private $conditions = array();
  private $url;
  private $yqlUseQuery = '';
  private $yqlSelectQuery = '';
  
  /**
   *
   * @param string $url 
   */
  public function __construct($url) {
    $this->setUrl($url);
  }
  
  /**
   * Return the id from the provided url
   * 
   * @param string $url
   * @return string
   */
  public function getId($url) {
    return end(split('/', trim($url, '/')));
  }
  
  /**
   *
   * @return string
   */
  public function getSource() {
    return $this->source;
  }

  /**
   *
   * @param string $source
   * @return WebService_QueryDelegate 
   */
  public function setSource($source) {
    $this->source = $source;
    return $this;
  }

  /**
   *
   * @return string
   */
  public function getUseTable() {
    return $this->useTable;
  }

  /**
   *
   * @param string $useTable
   * @return WebService_QueryDelegate 
   */
  public function setUseTable($useTable) {
    $this->useTable = $useTable;
    return $this;
  }
  
  /**
   *
   * @return array
   */
  public function getFields() {
    return $this->fields;
  }
  
  /**
   *
   * @param array $fields
   * @return WebService_QueryDelegate
   */
  public function setFields(array $fields = array()) {
    $this->fields = $fields;
    return $this;    
  }
  
  /**
   *
   * @return array
   */
  public function getConditions() {
    return $this->conditions;
  }
  
  /**
   *
   * @param array $conditions
   * @return WebService_QueryDelegate 
   */
  public function setConditions(array $conditions = array()) {
    $this->conditions = $conditions;
    return $this;
  }
  
  /**
   *
   * @return string
   */
  public function getUrl() {
    return $this->url;
  }
  
  /**
   *
   * @param string $url
   * @return WebService_QueryDelegate 
   */
  public function setUrl($url = NULL) {
    $this->url = $url;
    return $this;
  }

  /**
   *
   * @return string
   */
  public function getYqlUseQuery() {
    return $this->yqlUseQuery;
  }

  /**
   *
   * @param type $yqlUseQuery
   * @return WebService_QueryDelegate 
   */
  public function setYqlUseQuery($yqlUseQuery = NULL) {
    $this->yqlUseQuery = $yqlUseQuery;
    return $this;
  }

  /**
   *
   * @return string
   */
  public function getYqlSelectQuery() {
    return $this->yqlSelectQuery;
  }

  /**
   *
   * @param string $yqlSelectQuery
   * @return WebService_QueryDelegate 
   */
  public function setYqlSelectQuery($yqlSelectQuery = NULL) {
    $this->yqlSelectQuery = $yqlSelectQuery;
    return $this;
  }

  /**
   *
   * @return string
   */
  public function getYqlQuery() {
    return $this->getYqlUseQuery() . $this->getYqlSelectQuery();
  }
  
  /**
   * Format queries for later use
   * 
   * This method will be invoked in specific query delegates in order to prepare 
   * queries that will be invoked by the result delegates
   */
  public function prepareQueries() {
    $conditions   = $this->getConditions();
    $useQuery     = "USE '@table' AS @label;";
    $selectQuery  = 'SELECT @fields FROM @source WHERE @conditions;';

    if (empty($conditions)) {
      $queryConditions = array('1');
    }
    else {
      $queryConditions = array();
      foreach ($conditions as $key => $value) {
        $queryConditions[] = $key . "='" . call_user_func(explode('.', $value), $this->getUrl()) . "'";
      }
    }

    if ($this->getUseTable()) {
      $this->setYqlUseQuery(
        strtr($useQuery, array(
          '@table' => $this->getUseTable(),
          '@label' => $this->getSource(),
        ))
      );
    }

    $this->setYqlSelectQuery(
      strtr($selectQuery, array(
        '@fields'     => implode(', ', $this->getFields()),
        '@source'     => $this->getSource(),
        '@conditions' => implode(' AND ', $queryConditions),
      ))
    );
  }
  
}