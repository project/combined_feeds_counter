<?php

/**
 * @file
 * Define the WebService_Factory class
 */

/**
 * WebService_Factory class
 *
 * This class creates WebService classes and execute queries on given services.
 */
class WebService_Factory {
  /**
   * Create a WebService class and set its values
   * 
   * @param string $source_machine_name
   * @param array $source
   * @return WebService 
   */
  public static function create($source_machine_name = '', array $source = array()) {
    $url = variable_get($source_machine_name, NULL);

    if (empty($url)) {
      return NULL;
    }

    $webService      = new WebService();
    $queryDelegate   = 'WebService_QueryDelegate_' . ucfirst($source_machine_name);
    $resultsDelegate = 'WebService_ResultDelegate_' . ucfirst($source_machine_name);

    $webService
      ->setName(akh_get($source, 'title'))
      ->setDescription(akh_get($source, 'description'))
      ->setQueryDelegate(new $queryDelegate(variable_get($source_machine_name, NULL)))
      ->setResultDelegate(new $resultsDelegate());
      
    return $webService;
  }
  
  /**
   * Create an array of WebServices given the provided sources
   * 
   * @param array $sources
   * @return array of WebService 
   */
  public static function batchCreate(array $sources = array()) {
    $webServices = array();

    foreach ($sources as $source_machine_name => $source) {
      $webService = self::create($source_machine_name, $source);
      if (!empty($webService)) {
        $webServices[] = $webService;
      }
    }

    return self::SetFollowers($webServices);
  }
  
  /**
   * Get the followers from the proper service and set them in a WebService class.
   * 
   * @param array $services
   * @return array of WebService
   */
  public static function setFollowers(array $services = array()) {
    $stats   = new SubscriberStats($services);
    $results = $stats->executeQuery()->query->results->results;

    foreach ($results as $i => $result) {
      if (is_a($result, 'stdClass') && isset($services[$i])) {
        $services[$i]->setFollowers($result);
      }
    }

    return $services;
  }
}