<?php

/**
 * @file
 * Define the WebService_ResultDelegate interface
 */

/**
 * This class define two methods other result delegates must implement
 */
interface WebService_ResultDelegate {
  /**
   * Return the followers
   */
  public function getFollowers();
  
  /**
   * Set the followers
   */
  public function setFollowers(stdClass $result = NULL);
}
