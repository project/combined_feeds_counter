<?php

/**
 * @file
 * Define the WebService_QueryDelegate_Twitter class
 */

/**
 * Deal with YQL data structures and queries for Twitter
 */
class WebService_QueryDelegate_Twitter extends WebService_QueryDelegate {
  /**
   * Set needed values for querying the twitter graph
   * 
   * @param string $url 
   */
  public function __construct($url) {
    $this
      ->setUrl($url)
      ->setUseTable('http://www.datatables.org/twitter/twitter.users.xml')
      ->setSource('twitter.users')
      ->setFields(array('followers_count'))
      ->setConditions(array('id' => 'WebService_QueryDelegate_Twitter.getId'))
      ->prepareQueries();
  }
}