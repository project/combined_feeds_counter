<?php

/**
 * @file
 * Define the WebService_QueryDelegate_Facebook class
 */

/**
 * Deal with YQL data structures and queries for Facebook
 */
class WebService_QueryDelegate_Facebook extends WebService_QueryDelegate {
  /**
   * Set needed values for querying the facebook graph
   * 
   * @param string $url 
   */
  public function __construct($url) {
    $this
      ->setUrl($url)
      ->setUseTable('http://www.datatables.org/facebook/facebook.graph.xml')
      ->setSource('facebook.graph')
      ->setFields(array('likes'))
      ->setConditions(array('id' => 'WebService_QueryDelegate_Facebook.getId'))
      ->prepareQueries();
  }
}