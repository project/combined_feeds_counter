<?php

/**
 * @file
 * Define the WebService_QueryDelegate_Feedburner class
 */

/**
 * Deal with YQL data structures and queries for Feedburner
 */
class WebService_QueryDelegate_Feedburner extends WebService_QueryDelegate {
  /**
   * Set needed values for querying the feedburner graph
   * 
   * @param string $url 
   */
  public function __construct($url) {
    $this
      ->setUrl($url)
      ->setSource('xml')
      ->setFields(array('*'))
      ->setConditions(array('url' => 'WebService_QueryDelegate_Feedburner.getFeedburnerUrl'))
      ->prepareQueries();
  }
  
  /**
   *
   * @param string $url
   * @return string
   */
  public function getFeedburnerUrl($url) {
    return 'http://feedburner.google.com/api/awareness/1.0/GetFeedData?uri=' . $url;
  }
}