<?php

/**
 * @file
 * Define the WebService class
 */

/**
 * Describe a webservice
 */
class WebService {
  private $name;
  private $description;
  private $queryDelegate;
  private $resultDelegate;
  
  /**
   *
   * @return string
   */
  public function getName() {
    return $this->name;
  }
  
  /**
   *
   * @param string $name
   * @return WebService 
   */
  public function setName($name = NULL) {
    $this->name = $name;
    return $this;
  }
  
  /**
   *
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }
  
  /**
   *
   * @param string $description
   * @return WebService 
   */
  public function setDescription($description = NULL) {
    $this->description = $description;
    return $this;
  }
  
  /**
   *
   * @param WebService_QueryDelegate $queryDelegate
   * @return WebService 
   */
  public function setQueryDelegate(WebService_QueryDelegate $queryDelegate) {
    $this->queryDelegate = $queryDelegate;
    return $this;
  }

  /**
   *
   * @return WebService_ResultDelegate
   */
  public function getQueryDelegate() {
    return $this->queryDelegate;
  }

  /**
   *
   * @param WebService_ResultDelegate $resultDelegate
   * @return WebService 
   */
  public function setResultDelegate(WebService_ResultDelegate $resultDelegate) {
    $this->resultDelegate = $resultDelegate;
    return $this;
  }

  /**
   *
   * @return WebService_ResultDelegate 
   */
  public function getResultDelegate() {
    return $this->resultDelegate;
  }

  /**
   *
   * @return string
   */
  public function getYqlUseQuery() {
    return $this->getQueryDelegate()->getYqlUseQuery();
  }

  /**
   *
   * @param string $yqlUseQuery
   * @return WebService 
   */
  public function setYqlUseQuery($yqlUseQuery) {
    $this->getQueryDelegate()->setYqlUseQuery($yqlUseQuery);
    return $this;
  }

  /**
   *
   * @return string
   */
  public function getYqlSelectQuery() {
    return $this->getQueryDelegate()->getYqlSelectQuery();
  }

  /**
   *
   * @param string $yqlSelectQuery
   * @return WebService 
   */
  public function setYqlSelectQuery($yqlSelectQuery) {
    $this->getQueryDelegate()->setYqlSelectQuery($yqlSelectQuery);
    return $this;
  }

  /**
   *
   * @return stdClass
   */
  public function getFollowers() {
    return $this->getResultDelegate()->getFollowers();
  }

  /**
   *
   * @param stdClass $followers
   * @return WebService 
   */
  public function setFollowers($followers = NULL) {
    $this->getResultDelegate()->setFollowers($followers);
    return $this;
  }
  
}