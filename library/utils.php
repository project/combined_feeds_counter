<?php

/**
 * @file
 * Utility library
 *
 * This file contains some utilities such as string/array handlers and more
 */
 
/**
 * An array key getter
 *
 * Use this when you have one-level array,
 * ie: $arr = array('key1' => 'value1', 'key2' => 'value2');
 *
 * @param array  $array   array from which you want to get data
 * @param string $key     array key you want to extract
 * @param mixed  $default fallback value in case of non-existent key
 *
 * @return
 *   if the passed key of the array exists, return the corresponding value,
 *   else return the default value(if passed); as last resort return NULL.
 */
function akh_get(array $array, $key, $default = NULL) {
  return (isset($array[$key])) ? $array[$key] : $default;
}

/**
 * A recursive array key getter
 *
 * Use this when you have multiple-levels array,
 * ie: $arr = array('key1' => array('key3' => array('key4' => 'value4')),
 *                  'key2' => 'value2');
 *
 * @param array $array   array from which you want to get data
 * @param array $key     array containing index path of the key you want
 *                       to extract(ie: array('a', 'b', 'c')
 *                       will point to $array['a']['b']['c'])
 * @param mixed $default fallback value in case of non-existent key
 *
 * @return
 *   if the passed key of the array exists, return the corresponding value,
 *   else return the default value(if passed); as last resort return NULL.
 */
function akh_get_recursive(array $array, array $key, $default = NULL) {
  $inner_key = array_shift($key);
  return isset($array[$inner_key]) ?
    (empty($key) ?
      $array[$inner_key] :
      akh_get_recursive($array[$inner_key], $key, $default)) :
    $default;
}

/**
 * An array key setter
 *
 * Use this when you have one-level array,
 * ie: $arr = array('key1' => 'value1', 'key2' => 'value2');
 * 
 * @param array  $array array in which you want to set data
 * @param string $key   array key in which you want to store value
 * @param mixed  $value value you want to set
 *
 * @return
 *   if the key already exists, the old value of the array,
 *   else NULL
 */
function akh_set(array $array, $key, $value = NULL) {
  $old_value = akh_get($array, $key);
  $array[$key] = $value;
  return $old_value;
}

/**
 * A recursive array key setter
 *
 * Use this when you have multiple-levels array,
 * ie: $arr = array('key1' => array('key3' => array('key4' => 'value4')),
 *                  'key2' => 'value2');
 *
 * @param array &$array array in which you want to set data
 * @param array $key    array containing index path in which you want 
 *                      to store value(ie: array('a', 'b', 'c')
 *                      will point to $array['a']['b']['c'])
 * @param mixed $value  value you want to set
 *
 * @return
 *   if the key already exists, the old value of the array,
 *   else NULL
 */
function akh_set_recursive(array &$array, array $key = array(), $value = NULL) {
  $old_value = akh_get_recursive($array, $key);
  if (($inner_key = array_shift($key))) {
    if (!isset($array[$inner_key])) {
      $array[$inner_key] = array();
    }
    akh_set_recursive($array[$inner_key], $key, $value);
  } else {
    $array = $value;
  }
  return $old_value;
}
