<?php

/**
 * @file
 * Define the SubscriberStats class
 */

/**
 * SubscriberStats class
 *
 * This class prepares and executes yql queries 
 * gathered from the provided array of WebService classes.
 */
class SubscriberStats {
	private $yahooQueryApiUrl = array(
	  'url' => 'http://query.yahooapis.com/v1/public/yql',
	  'params' => array(
	    'q'           => '@useQueries SELECT * FROM yql.query.multi WHERE queries = "@selectQueries";',
	    'format'      => 'json',
	    'diagnostics' => 'FALSE',
	    'env'         => 'http://datatables.org/alltables.env',
	  ),
	);

  /**
   * Class constructor
   *
   * Get the specific query for each provided service and build a container yql query.
   *
   * @param array $services
   *   An array of WebService objects
   *
   * @return
   */
	public function __construct(array $services = array()) {
		$yqlUseQueries    = array();
    $yqlSelectQueries = array();

    foreach ($services as $service_machine_name => $service) {
      $yqlUseQueries[]    = $service->getYqlUseQuery();
      $yqlSelectQueries[] = $service->getYqlSelectQuery();
    }

    $this->yahooQueryApiUrl['params']['q'] =
      strtr($this->yahooQueryApiUrl['params']['q'], array(
        '@useQueries'    => implode(' ', $yqlUseQueries),
        '@selectQueries' => implode(' ', $yqlSelectQueries),
      ));
	}

  /**
   * Compose the request url and perform it
   *
   * @return
   *   json text containing yql query results
   */
	public function executeQuery() {
	  $params = array();
	  foreach ($this->yahooQueryApiUrl['params'] as $param => $value) {
	    $params[] = $param . '=' . urlencode($value);
    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $this->yahooQueryApiUrl['url'] . '?' . implode('&', $params));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    if (($result = curl_exec($ch)) === FALSE) {
      return (object) array(
        'error' => (object) array(
          'lang' => 'en-US',
          'description' => 'Curl error',
        ),
      );
    };
    curl_close($ch);
    
	  return json_decode($result);
  }
}
