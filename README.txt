
-- SUMMARY --

The Drupal Combined feeds counter module enables a block showing the
aggregate counter of the followers on the configured social networks.

For a full description of the module, visit the project page:
  http://drupal.org/project/combined_feeds_counter

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/combined_feeds_counter


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Customize the block settings as the address of the social networks pages and
  the access permissions in Home >> Administer >> Site building >> Blocks.


-- CUSTOMIZATION --

* TODO


-- TROUBLESHOOTING --

* If the counter tells 0 followers, check the following:

  - Address are written like this:
    - Facebook: http://www.facebook.com/smashmag
    - Twitter: http://www.twitter.com/Tutorialzine
    - Google Feed Burner: http://feeds.feedburner.com/Tutorialzine

  - remind that in this release just Facebook, Twitter and Feedburner are implemented.


-- FAQ --

Q: Counter isn' t working. Why?

A: If what' s written in Troubleshooting section doesn't work, 
   please open a ticket in the issue list.


-- CONTACT --

Current maintainers:
* Claudio Beatrice (omissis) - http://drupal.org/user/399318

This project has been sponsored by: http://nois3lab.it
